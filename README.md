# hun-tools-bin

Contains Linux binaries of several open source Hungarian NLP processing tools and their respective data files: 

- `huntoken 1.6` for tokenisation
- `hunmorph` with the `morphdb_hu` binary lexicon for morphological analysis
- `hunpos` with the `hu_szeged_kr.model` for part-of-speech tagging.

See also: https://github.com/mmihaltz/hunlp-pipeline

Maintainer: Marton Mihaltz

## Installation

1. Get this repo:
  ```git clone https://bitbucket.org/mmihaltz/hun-tools-bin.git```

2. Unzip:
  ```
  cd hun-tools-bin ;
  unzip hun-tools.zip
  ```

3. Set your PATH so it includes the `huntoken-1.6/bin` directory, eg.
  ```
  export PATH=$PATH:/your/path/to/hun-tools-bin/huntoken-1.6/bin
  ```

*NOTE*: the binaries in the package are built for i386 (32-bit) Linux systems. If you have a 64-bit system, follow your distribution's instructions on how to run 32-bit binaries; eg. for Ubuntu 14.04 follow [these instructions](https://askubuntu.com/questions/454253/how-to-run-32-bit-app-in-ubuntu-64-bit)